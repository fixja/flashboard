from setuptools import setup, find_packages

setup(
    name='flashboard',
    packages=find_packages(exclude=['test_app']),
    author='Gary Reynolds',
    author_email='gary@touch.asn.au',
    description='',
    url='https://bitbucket.org/fixja/flashboard',
    install_requires=[
        'vitriolic[competition]',
    ],
    setup_requires=["setuptools_scm"],
    use_scm_version=True,
    include_package_data=True,
    zip_safe=False,
)
